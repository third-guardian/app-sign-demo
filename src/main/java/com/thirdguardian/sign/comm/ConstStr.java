package com.thirdguardian.sign.comm;

/**
 * 签名需要用到的字段
 *
 * @author Administrator
 */
public class ConstStr {
    public static final String FIELD_SIGN = "sign";
    public static final String FIELD_SIGN_TYPE = "signType";
    public static final String TIMESTAMP = "timestamp";
    public static final String NONCE_STR = "nonceStr";
    public static final String APP_ID = "appId";
    public static final String TIME_FORMAT_STR = "yyyy-MM-dd HH:mm:ss";
    public static final String SIGN_AUTH_TYPE = "MUERXISIGNV3-SHA256-RSA2048";
    public static final String SIGN_AUTH_TYPE_V4 = "MUERXISIGNV4-SHA256-RSA2048";
    public static final String SIGN_HEADER_PARAM_NAME = "Authorization";
    public static final String SIGN_NATURE = "signature";
    public static final String SIGN_URL_SUFFIX = "urlSuffix";
}
