# 说明

此签名是仿微信支付v2的签名写的签名方法，

# 关于签名

签名是各应用之间，进行通讯时的身份验证方法，与token不同，不是在每个请求的头部携带token，而是将每次请求的参数，进行签名打包后，再向目标地址发送请求。
请求的方式，仅支持`post`。



# 签名的条件
如果要进行签名和签名验证，则需要有我方提供的**License**: `appId`和`secretKey`。

# 生成签名
1. 构建待签名的参数，创建一个json对象，以键值对作为参数。
2签名算法支持：HMAC-SHA256, MD5。
3调用方法：`SignUtils.singRequestParams(jsonParams, licensePair, signMethod)`;
   > 说明：
   >
   > jsonParams: 为构建的json参数对象
   >
   > licensePair: 为我方签发的license，需要构建为一对象： SignUtil.LicensePair
   >
   > signMethod: 为签名方式：SignUtil.SignType
   >
   > 返回值: 为包含签名的请求参数

# 验证签名
1. 调用方法：`SignUtil.verifySign(jsonParams, licensePair)`;
   > 说明：
   >
   > jsonParams: post请求传过来的参数
   >
   > licensePair: 为我方签发的license，需要构建为一对象： SignUtil.LicensePair
   >
   > 返回值: 为true(通过验证)、false(未通过验证)

**具体示例，请查看test**

# 签名时的主意事项
1. 为了避免出现不可预知的情况，待签名的参数json对象，仅支持1个层级关系，不支持对象嵌套。
2. 为了避免参数键值与签名所需的键值冲突，在接口参数设计时，一定不要使用如参数：
    + `timestamp`
    + `nonce_str`
    + `sign_method`
    + `app_id`
    + `sign`