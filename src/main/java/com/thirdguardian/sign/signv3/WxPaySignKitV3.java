package com.thirdguardian.sign.signv3;

import com.thirdguardian.sign.comm.ConstStr;

import java.security.PrivateKey;
import java.security.PublicKey;


/**
 * <p>IJPay 让支付触手可及，封装了微信支付、支付宝支付、银联支付常用的支付方式以及各种常用的接口。</p>
 *
 * <p>不依赖任何第三方 mvc 框架，仅仅作为工具使用简单快速完成支付模块的开发，可轻松嵌入到任何系统里。 </p>
 *
 * <p>IJPay 交流群: 723992875</p>
 *
 * <p>Node.js 版: https://gitee.com/javen205/TNWX</p>
 *
 * <p>微信支付工具类</p>
 *
 * @author Javen
 */
public class WxPaySignKitV3 {

    /**
     * 构建 v3 接口所需的 Authorization
     *
     * @param urlSuffix  可通过 WxApiType 来获取，URL挂载参数需要自行拼接
     * @param appId      商户Id
     * @param privateKey 商户私钥
     * @param body       接口请求参数
     * @param nonceStr   随机字符库
     * @param timestamp  格式化后的时间
     * @return {@link String} 返回 v3 所需的 Authorization
     * @throws Exception 异常信息
     */
    public static String buildAuthorization(String appId, String urlSuffix, PrivateKey privateKey, String body, String nonceStr,
                                            String timestamp) throws Exception {
        // 构建签名参数
        String buildSignMessage = PayKit.buildSignMessage(appId, urlSuffix, timestamp, nonceStr, body);
        String signature = PayKit.createSign(buildSignMessage, privateKey);
        // 根据平台规则生成请求头 authorization
        return PayKit.getAuthorization(appId, urlSuffix, nonceStr, timestamp, signature, ConstStr.SIGN_AUTH_TYPE);
    }


    /**
     * 验证签名
     *
     * @param signature 待验证的签名
     * @param body      应答主体
     * @param nonceStr  随机串
     * @param timestamp 时间戳
     * @param publicKey 微信支付平台公钥
     * @return 签名结果
     * @throws Exception 异常信息
     */
    public static boolean verifySignature(String appId, String urlSuffix, String signature, String body, String nonceStr, String timestamp, String publicKey) throws Exception {
        String buildSignMessage = PayKit.buildSignMessage(appId, urlSuffix, timestamp, nonceStr, body);
        return RsaKit.checkByPublicKey(buildSignMessage, signature, publicKey);
    }

    /**
     * 验证签名
     *
     * @param signature 待验证的签名
     * @param body      应答主体
     * @param nonceStr  随机串
     * @param timestamp 时间戳
     * @param publicKey {@link PublicKey} 微信支付平台公钥
     * @return 签名结果
     * @throws Exception 异常信息
     */
    public static boolean verifySignature(String appId, String urlSuffix, String signature, String body, String nonceStr, String timestamp, PublicKey publicKey) throws Exception {
        String buildSignMessage = PayKit.buildSignMessage(appId, urlSuffix, timestamp, nonceStr, body);
        return RsaKit.checkByPublicKey(buildSignMessage, signature, publicKey);
    }
}
