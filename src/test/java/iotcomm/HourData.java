package iotcomm;

public class HourData {
    /**
     * 时间 1 代表一点
     */
    private int time;
    /**
     * 对应时间的数据
     */
    private int data;

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public HourData(int time, int data) {
        this.time = time;
        this.data = data;
    }

    @Override
    public String toString() {
        return "comm.HourData{" +
                "time=" + time +
                ", data=" + data +
                '}';
    }
}




