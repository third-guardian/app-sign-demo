package iotcomm;

import java.util.List;

public class RepOneDayData extends RspBaseEntity {

    public RepOneDayData(int cmd) {
        super(cmd);
    }

    /**
     * 数据的时间
     */
    private String dataTime;
    /**
     * 表具时间
     */
    private String deviceTime;
    /**
     * 倍率
     */
    private int magnification;


    private List<HourData> dataList;

    public String getDeviceTime() {
        return deviceTime;
    }

    public void setDeviceTime(String deviceTime) {
        this.deviceTime = deviceTime;
    }

    public int getMagnification() {
        return magnification;
    }

    public void setMagnification(int magnification) {
        this.magnification = magnification;
    }

    public String getDataTime() {
        return dataTime;
    }

    public void setDataTime(String dataTime) {
        this.dataTime = dataTime;
    }

    public List<HourData> getDataList() {
        return dataList;
    }

    public void setDataList(List<HourData> dataList) {
        this.dataList = dataList;
    }


    @Override
    public String toString() {
        return "RepOneDayData{" +
                "dataTime='" + dataTime + '\'' +
                ", deviceTime='" + deviceTime + '\'' +
                ", magnification=" + magnification +
                ", dataList=" + dataList +
                '}';
    }
}
