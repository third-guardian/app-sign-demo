package iotcomm;

public class ParseIntEntity {
    /**
     * 解析截取后的字符串
     */
    private String clipStr;
    /**
     * 解析出的结果
     */
    private int data;

    public String getClipStr() {
        return clipStr;
    }

    public void setClipStr(String clipStr) {
        this.clipStr = clipStr;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public ParseIntEntity(String clipStr, int dataLen) {
        this.clipStr = clipStr;
        this.data = dataLen;
    }
}
