package iotcomm;

public class ParseStringEntity {
    /**
     * 解析截取后的字符串
     */
    private String clipStr;
    /**
     * 解析出的结果
     */
    private String resultStr;

    public ParseStringEntity(String clipStr, String resultStr) {
        this.clipStr = clipStr;
        this.resultStr = resultStr;
    }

    public String getClipStr() {
        return clipStr;
    }

    public void setClipStr(String clipStr) {
        this.clipStr = clipStr;
    }

    public String getResultStr() {
        return resultStr;
    }

    public void setResultStr(String resultStr) {
        this.resultStr = resultStr;
    }
}