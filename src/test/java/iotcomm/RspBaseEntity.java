package iotcomm;

public class RspBaseEntity {
    private int cmd;


    /**
     * 从站编码(表具编号)
     */
    private String stationCode;

    public int getCmd() {
        return cmd;
    }

    public void setCmd(int cmd) {
        this.cmd = cmd;
    }
    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    @Override
    public String toString() {
        return "RspBaseEntity{" +
                "\n" + "从站编码(表具编号)='" + stationCode + '\'' +
                '}';
    }

    public RspBaseEntity(int cmd) {
        this.cmd = cmd;
    }
}
