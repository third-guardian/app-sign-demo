package iotcomm;

public class RepWaterData extends RspBaseEntity {

    /**
     * 表具时间
     */
    private String deviceTime;
    /**
     * 倍率
     */
    private int magnification;
    /**
     * 水量
     */
    private int count;
    /**
     * 阀门状态
     */
    private int valveStatus;
    /**
     * 电压
     */
    private int battery;
    /**
     * 信号强度
     */
    private int rssi;
    /**
     * 上报周期
     */
    private int period;
    /**
     * 设置的上报时间
     */
    private String repTime;
    /**
     * 传感器状态
     */
    private int errSenser;
    /**
     * 电池电压状态
     */
    private int errBattery;
    /**
     * 信号强度等级
     */
    private int errRssi;

    public RepWaterData(int cmd) {
        super(cmd);
    }

    public String getDeviceTime() {
        return deviceTime;
    }

    public void setDeviceTime(String deviceTime) {
        this.deviceTime = deviceTime;
    }

    public int getMagnification() {
        return magnification;
    }

    public void setMagnification(int magnification) {
        this.magnification = magnification;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getValveStatus() {
        return valveStatus;
    }

    public void setValveStatus(int valveStatus) {
        this.valveStatus = valveStatus;
    }

    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public String getRepTime() {
        return repTime;
    }

    public void setRepTime(String repTime) {
        this.repTime = repTime;
    }

    public int getErrSenser() {
        return errSenser;
    }

    public void setErrSenser(int errSenser) {
        this.errSenser = errSenser;
    }

    public int getErrBattery() {
        return errBattery;
    }

    public void setErrBattery(int errBattery) {
        this.errBattery = errBattery;
    }

    public int getErrRssi() {
        return errRssi;
    }

    public void setErrRssi(int errRssi) {
        this.errRssi = errRssi;
    }

    @Override
    public String toString() {
        return "RepWaterData{" +
                "\n" + "表具时间='" + CommandUtils.getDateTime(deviceTime) + '\'' +
                "\n" + ", 倍率=" + magnification +
                "\n" + ", 累积量=" + count +
                "\n" + ", 阀门状态=" + valveStatus +
                "\n" + ", 电池电压=" + battery +
                "\n" + ", 信号强度=" + rssi +
                "\n" + ", 上报周期=" + period +
                "\n" + ", 设置的上报时间='" + CommandUtils.getDateTime(repTime) + '\'' +
                "\n" + ", 传感器状态=" + errSenser +
                "\n" + ", 电池电压状态=" + errBattery +
                "\n" + ", 信号强度等级=" + errRssi +
                '}';
    }
}
