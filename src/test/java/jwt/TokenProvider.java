package jwt;

import cn.hutool.core.util.IdUtil;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

import java.security.Key;
import java.util.Date;

public class TokenProvider {

    private JwtParser jwtParser;
    private JwtBuilder jwtBuilder;

    public void createToken() {
        byte[] keyBytes = Decoders.BASE64.decode(JWTConst.base64Secret);
        Key key = Keys.hmacShaKeyFor(keyBytes);
        jwtParser = Jwts.parserBuilder()
                .setSigningKey(key)
                .build();
        jwtBuilder = Jwts.builder()
                .signWith(key, SignatureAlgorithm.HS512);
        String token = jwtBuilder
                // 加入ID确保生成的 Token 都不一致
                .setId(IdUtil.simpleUUID())
                .claim("user", "admin")
                .setSubject("admin")
                // 设置过期时间
                .setExpiration(new Date(System.currentTimeMillis() + 60 * 1000))
                .compact();
        System.out.println(token);
    }
}
