import cn.hutool.json.JSONUtil;
import com.thirdguardian.sign.signv1.SignUtils;
import com.thirdguardian.sign.signv3.RsaKit;
import com.thirdguardian.sign.signv3.SignV3Utils;
import com.thirdguardian.sign.signv4.SignV4Utils;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

public class SignTest {
    public static void main(String[] args) throws Exception {
        testV4();
    }


    static void testV2() throws Exception {
        SignUtils.LicensePair pair = new SignUtils.LicensePair("12312313", "1231313123");
        Map<String, Object> paramBre = new HashMap<>();
        Map<String, Object> params = SignUtils.singRequestParams(paramBre, pair, SignUtils.SignType.MD5);
        System.out.println(params);
        Boolean res = SignUtils.verifySign(params, pair);
        System.out.println(res);
    }

    static void testV3() throws Exception {
        Map<String, String> params = new HashMap<>();
        params.put("aaa","131313");
        params.put("bbb","3333333");
        String body = JSONUtil.toJsonStr(params);

        PrivateKey privateKey = RsaKit.loadPrivateKey(PRIVATE_KEY);
        String signature = SignV3Utils.buildSignToken("12312313", "/api/verify", privateKey, body);
        System.out.println(signature);
        PublicKey publicKey = RsaKit.loadPublicKey(PUBILC_KEY);
        boolean res = SignV3Utils.verifySignToken(signature, body, publicKey);
        System.out.println(res);
    }

    static void testV4() throws Exception {
        Map<String, String> params = new HashMap<>();
        params.put("code","0834Ldml2wLFh84OwVll2KqQGp44Ldmc");
        String body = JSONUtil.toJsonStr(params);

        String signature = SignV4Utils.buildSignToken("wx478d426df71f65d4", "/wechat/miniAppAccounts/getUserInfo", "56c0022df177076ac464e1c9bb906be1", body);
        System.out.println(signature);
        boolean res = SignV4Utils.verifySignToken(signature, body, "56c0022df177076ac464e1c9bb906be1");
        System.out.println(res);
    }

    private final static String PRIVATE_KEY = "MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAxN9kChhHbM9HQbL5mSC0uj1e75Qy/clsWafRlSgkDI/DuqRx9vr5KDNi7vPCz8cagLLiW0q6bubN+wFyuizcyQIDAQABAkAxGzOd3P574pv4hEcD7geGaOZ0RfgOG57eIiEB3nhXdVfMFi94OuzXvA5zt+dN4BKHWuF43uHWRqYfVYuDlq+BAiEA/t4nH0N/2urS33PYe+1xx3/tU5P0vI4RHEcE16yDvtkCIQDFv0iB8SEt89JkkiMrW03BAqkC94Brk58cjdPgHQ03cQIhANxigPXZyk8kyx+J7uKKmd5m0nN49yGsv0Lybk+Iz4tRAiBshkkFv08FSB28QhN5FPe8KHiW/zwEThTxd8jC407AkQIgH5B2s+liwhE+ebyjJrYo/tAeMXbFH0hBucR+eXMCf7M=";

    private final static String PUBILC_KEY = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMTfZAoYR2zPR0Gy+ZkgtLo9Xu+UMv3JbFmn0ZUoJAyPw7qkcfb6+SgzYu7zws/HGoCy4ltKum7mzfsBcros3MkCAwEAAQ==";
}
