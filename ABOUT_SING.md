# 签名方式分为 v2 和 v3

## v2 仿微信支付v2版本签名方法

## v3 仿微信支付v3版本方法

### v3 前端demo

```ts
// 接口签名工具
import jsrsasign from 'jsrsasign'
import format from 'date-fns/format';
// TODO: 正式部署的时候，需要更换公钥和私钥
// 接口签名私钥
export const privateKey = "MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAxN9kChhHbM9HQbL5mSC0uj1e75Qy/clsWafRlSgkDI/DuqRx9vr5KDNi7vPCz8cagLLiW0q6bubN+wFyuizcyQIDAQABAkAxGzOd3P574pv4hEcD7geGaOZ0RfgOG57eIiEB3nhXdVfMFi94OuzXvA5zt+dN4BKHWuF43uHWRqYfVYuDlq+BAiEA/t4nH0N/2urS33PYe+1xx3/tU5P0vI4RHEcE16yDvtkCIQDFv0iB8SEt89JkkiMrW03BAqkC94Brk58cjdPgHQ03cQIhANxigPXZyk8kyx+J7uKKmd5m0nN49yGsv0Lybk+Iz4tRAiBshkkFv08FSB28QhN5FPe8KHiW/zwEThTxd8jC407AkQIgH5B2s+liwhE+ebyjJrYo/tAeMXbFH0hBucR+eXMCf7M=";
// TODO：正式部署时，根据不用的应用进行切换
// 小程序Id
export const appId = "wx478d426df71f65d4";


export function makeSign(urlSuffix: string, body: any) {
    // new一个RSA对象
    let rsa = new jsrsasign.RSAKey();
    // SHA256withRSA私钥
    const k = `-----BEGIN PRIVATE KEY-----`
        + privateKey +
        `-----END PRIVATE KEY-----`
    // 将私钥 转成16进制
    rsa = jsrsasign.KEYUTIL.getKey(k);
    // 采用SHA256withRSA进行加密
    const sig = new jsrsasign.KJUR.crypto.Signature({
        alg: 'SHA256withRSA'
    });
    // 算法初始化
    sig.init(rsa);
    const timeStr = format(new Date(), 'yyyy-MM-dd HH:mm:ss');
    const nonceStr = randomString(16);
    const bodyStr = JSON.stringify(body);

    const list = [appId, urlSuffix, timeStr, nonceStr, bodyStr];
    const signMessage = buildSignMessage(list);
    // 对签名内容进行加密
    sig.updateString(signMessage)
    // 加密后的16进制转成base64，这就是签名了
    const signature = jsrsasign.hextob64(sig.sign());
    const signParams = {
        appId: appId,
        nonceStr: nonceStr,
        signature: signature,
        timestamp: timeStr,
        urlSuffix: urlSuffix
    }
    return makeAuthStr(signParams);
}

/**
 * 构建签名字符串
 * @param list
 */
function buildSignMessage(list: string[]) {
    let str = "";
    list.forEach(item => {
        str = str + item + "\n";
    });
    return str;
}

/**
 * 生成签名内容
 * @param params
 */
function makeAuthStr(params: any) {
    const keys = Object.keys(params);
    let str = "MUERXISIGNV4-SHA256-RSA2048 ";
    let strArr = keys.map(key => {
        return key + `="` + params[key] + `"`;
    });
    return str + strArr.join(',');
}

/**
 * 生成长度限定的随机数
 * @param length
 */
function randomString(length) {
    const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

```
### v4 在v3的基础上，修改签名加密算法

